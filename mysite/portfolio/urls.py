from django.conf.urls import url
from . import views
from django.views.generic import ListView, DetailView
from blog.models import Post

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^resume/$', views.resume, name='resume'),
    url(r'^about/$', views.about, name='about'),
    url(r'^projects/$', views.projects, name='projects'),
]