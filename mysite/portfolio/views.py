from django.http import HttpResponse
from django.shortcuts import render

def index(request):
    return render(request, 'portfolio/home.html')

def resume(request):
    return render(request, 'portfolio/resume.html')

def blog(request):
    return render(request, 'blog/blog.html')

def projects(request):
    return render(request, 'portfolio/projects.html')

def about(request):
    return render(request, 'portfolio/about.html')